/// @file
/// @brief Module works with section extraction

#include "section_extractor.h"
#include "util.h"

const char* const read_status_message[] = {
        [READ_OK] = "Read successfully",
        [READ_ERROR] = "Failed to read data",
        [POINTER_ERROR] = "Failed to move the pointer",
        [MAGIC_NUMBER_ERROR] = "The magic number is wrong",
        [MEMORY_ERROR] = "Failed to allocate the memory",
        [FILE_ERROR] = "Failed to read from PE file. The file pointer is null"
};

/// @brief Extract PE file's section by its name and write into the file
/// @param[in] in Source file
/// @param[in] out Target file
/// @param[in] section_name  Name of the section
/// @return true in case of success
bool extract_PE_section(FILE* const in, FILE* const out, const char* const section_name) {
    struct PEFile PE_file = {0};
    enum read_status result = read_PE_headers(in, &PE_file);
    println_err(read_status_message[result]);
    if (result == READ_OK) {
        bool res = write_section_to_file(in, out, find_section_by_name(section_name, &PE_file));
        free(PE_file.section_headers);
        return res;
    }
    if (result != MEMORY_ERROR) {
        free(PE_file.section_headers);
    }
    return false;
}
