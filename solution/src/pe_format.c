/// @file
/// @brief Module works with a PE format

#include "pe_format.h"
#include "util.h"

/// @brief Read PE file headers into the structure
/// @param[in] in Input file
/// @param[in] PE_file Structure describing file headers
/// @return true in case of success
enum read_status read_PE_headers (FILE* const in, struct PEFile* const PE_file) {
    if (in) {
        if (fseek(in, MAIN_OFFSET, SEEK_SET) != 0) {
            return POINTER_ERROR;
        }
        if (!fread(&PE_file->header_offset, sizeof(PE_file->header_offset), 1, in)) {
            return READ_ERROR;
        }
        if (fseek(in, PE_file->header_offset, SEEK_SET) != 0) {
            return POINTER_ERROR;
        }
        if (!fread(&PE_file->magic, sizeof(PE_file->magic), 1, in)) {
            return READ_ERROR;
        }
        if (PE_file->magic != MAGIC) {
            return MAGIC_NUMBER_ERROR;
        }

        if (!fread(&PE_file->header, sizeof(struct PEHeader), 1, in)) {
            return READ_ERROR;
        }

        if (fseek(in, PE_file->header.sizeOfOptionalHeader, SEEK_CUR) != 0) {
            return POINTER_ERROR;
        }

        PE_file->section_headers = malloc(PE_file->header.numberOfSections * sizeof(struct SectionHeader));
        if (!PE_file->section_headers) {
            return MEMORY_ERROR;
        }

        for (uint16_t i = 0; i < PE_file->header.numberOfSections; i++) {
            if (!fread(&PE_file->section_headers[i], sizeof(struct SectionHeader), 1, in)) {
                return READ_ERROR;
            }
        }
        return READ_OK;
    }
    return FILE_ERROR;
}

/// @brief Find PE file's section by its name
/// @param[in] section_name Name of the section
/// @param[in] file Structure with all file's headers
/// @return pointer to the structure in case of success, null otherwise
struct SectionHeader* find_section_by_name(const char* const section_name, const struct PEFile* const file) {
    for (uint16_t i = 0; i < file->header.numberOfSections; i++) {
        if (strncmp((char*)file->section_headers[i].name, section_name, NAME_FIELD_SIZE) == 0) {
            return file->section_headers + i;
        }
    }
    return NULL;
}

/// @brief Write PE file's section into the file
/// @param[in] in Source file
/// @param[in] out Target file
/// @param[in] sectionHeader Structure with section's headers
/// @return true in case of success
bool write_section_to_file(FILE* const in, FILE* const out, const struct SectionHeader* const sectionHeader) {
    if (sectionHeader) {
        if (fseek(in, sectionHeader->pointerToRawData, SEEK_SET) != 0) {
            println_err("Failed to move the pointer");
            return false;
        }
        char data[BUFFER_SIZE];
        uint32_t remaining = sectionHeader->SizeOfRawData;
        while (remaining) {
            if (remaining >= sizeof(data)) {
                size_t result = fread(data, sizeof(data), 1, in);
                if (!result) {
                    println_err("Failed to read the section data");
                    return false;
                }
                result = fwrite(data, sizeof(data), 1, out);
                if (!result) {
                    println_err("Failed to write the section data");
                    return false;
                }
                remaining -= sizeof(data);
            } else {
                size_t result = fread(data, remaining, 1, in);
                if (!result) {
                    println_err("Failed to read the section data");
                    return false;
                }
                result = fwrite(data, remaining, 1, out);
                if (!result) {
                    println_err("Failed to write the section data");
                    return false;
                }
                remaining = 0;
            }
        }
        return true;
    }
    println_err("Failed to write the section data. Section header is null");
    return false;
}
