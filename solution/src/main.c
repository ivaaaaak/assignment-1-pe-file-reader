/// @file 
/// @brief Main application file

#include "section_extractor.h"
#include "util.h"

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
  fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
    if (argc < 4) {
        usage(stdout);
        return 0;
    } else {
        FILE *in = fopen(argv[1], "rb");
        if (!in) {
            println_err("Failed to open input file");
            if (fclose(in) != 0) {
                println_err("Failed to close input file");
            }
            return 1;
        }
        FILE *out = fopen(argv[3], "wb");
        if (!out) {
            println_err("Failed to open output file");
            if (fclose(in) != 0) {
                println_err("Failed to close input file");
            }
            if (fclose(out) != 0) {
                println_err("Failed to close output file");
            }
            return 1;
        }
        bool res = extract_PE_section(in, out, argv[2]);
        if (fclose(in) != 0) {
            println_err("Failed to close input file");
        }
        if (fclose(out) != 0) {
            println_err("Failed to close output file");
        }
        return !res;
    }
}
