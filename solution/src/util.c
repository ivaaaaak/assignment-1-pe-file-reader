/// @file
/// @brief File contains utility work with standard output streams

#include "util.h"

/// @brief Print error into stderr
/// @param[in] err Error message
void println_err(const char* const err) {
    fprintf(stderr, "%s \n", err);
}

/// @brief Print message into stdout
/// @param[in] msg Message
void println(const char* const msg) {
    fprintf(stdout, "%s \n", msg);
}

