/// @file
/// @brief File contains utility work with standard output streams
#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>

/// @brief Print error into stderr
/// @param[in] err Error message
void println_err(const char* err);

/// @brief Print message into stdout
/// @param[in] msg Message
void println(const char* msg);

#endif
