/// @file
/// @brief Module works with section extraction
#ifndef SECTION_EXTRACTOR_H
#define SECTION_EXTRACTOR_H

#include "pe_format.h"

/// @brief Extract PE file's section by its name and write into the file
/// @param[in] in Source file
/// @param[in] out Target file
/// @param[in] section_name  Name of the section
/// @return true in case of success
bool extract_PE_section(FILE* in, FILE* out, const char* section_name);

#endif
